/*global define, _, $, jQuery, alert, console, chrome, Blob, FileReader, saveAs*/
/*jslint white: true */
/*jslint vars: true*/

define(['require','app/templates', 'jquery', 'FileSaver', 'backbone', 'underscore', 'q'], function(require, templates, $, FileReader, Backbone, _, Q, gtp){

'use strict';

//extend input adding enterKey method
$.fn.enterKey = function (fnc, mod) {
    return this.each(function () {
        $(this).keypress(function (ev) {
            var keycode = (ev.keyCode)? ev.keyCode : ev.which;
            if ((keycode === '13' || keycode === '10') && (!mod || ev[mod + 'Key'])) {
                fnc.call(this, ev);
            }
        });
    });
};

/////////////////////////jparameters namespace
window.jParameters = {};
var jParameters = window.jParameters;

    
jParameters.createQualifiedName = function (rootName, name) {
    //replace spaces in name
    name = name.replace(" ", "_");
    
    if (rootName.length === 0) {
        return name;
    } else {
        return rootName + "-" + name;
    }
};

    
jParameters.Param = Backbone.Model.extend({

    default : { name : "", data : {}, value : "", description : ""},
    
    initialize : function(){
        this.set({  originalValue : this.get("name")   });
    }
}); 
    
jParameters.ParamSet = Backbone.Collection.extend({
    model : jParameters.Param
});
    
jParameters.breakDescription = function(value){
    var iii = value.search("\\. ");
    var valueShort = (iii > 0)? value.substr(0, iii) : value;
    var value = value.replace(". ", ".<br/>");
    return {tooltip : value, description : valueShort};
};
    
jParameters.Group = Backbone.Model.extend({
    defaults : { data : {}, name : "", description : "" },
    initialize: function() {
        this.parameters = new jParameters.ParamSet;
        this.subGroups = new jParameters.GroupSet;
        /////////////////////////
        //go through data elements and append parameters
        var that = this;
        var subGroups = [];
        var properties = [];
        var descriptions = [];
        $.each(this.get("data"), function (name, value) {
            var modelAttributes;
            if ($.isPlainObject(value)) {
                modelAttributes = {  "name" : name, "data" : value};
                if (value["_description"]){
                    var des = jParameters.breakDescription(value["_description"]);
                    modelAttributes.tooltip = des.tooltip;
                    modelAttributes.description = des.description;    
                }
                subGroups.push(modelAttributes);
            } else { 
                //special case for descriptions
                if (name.match(/_/) && typeof value === "string"){
                    //skip it
                } else {
                    modelAttributes = { "model" : that.get("data"),  "name" : name, "value" : value};
                    var candidateD = that.get("data")[name + "_description"];
                    if (candidateD){
                        des = jParameters.breakDescription(candidateD);
                        modelAttributes.tooltip = des.tooltip;
                        modelAttributes.description = des.description;
                    }
                    properties.push(modelAttributes);
                }
            }
        });
        
        this.parameters.add(properties);
        this.subGroups.add(subGroups);    
        
        ///////////////////////
        console.info("group "+ this.get("name") + " ready");
    },

});    

    
    
jParameters.GroupSet = Backbone.Collection.extend({
   
    model : jParameters.Group,
    
    initialize: function(){
        console.info("groupset collection ready");
    },


});       
    
    
 jParameters.ListModel = jParameters.Group.extend({
     defaults : {
            name : "Parameters",
            description : "",
            data : {},
            filterQuery : "",
            originalData : [] },
                                                  
     prepareData : function(d){
        //set list values
        this.set({data : d, filterQuery : "", originalData : d});
        //set group values
        this.set({data : d, name : "Parameters", desc: ""});
        
        this.initialize();
     },
    
    
     
    
});    

    
    
    
jParameters.listModel = new jParameters.ListModel();    
    
    
    

////////////////VIEWS
        
    
jParameters.ParamView = Backbone.View.extend({
    el: '<div></div>',
    template : templates.param,
 
    initialize: function(){
        this.render();
    },
    
    render: function(){
        var template = this.template({name: this.model.get("name"), 
                                      value : this.model.get("name"), 
                                      description : this.model.get("description")});
        this.$el.html(template);
        return this;
    }
});    
    
     
    
    
    
//backbone parameter view
jParameters.ParamSetView = Backbone.View.extend({
    el: '<div></div>',
    template : templates.param,
 
    initialize: function(){
        var that = this;
        this.views = [];

        this.collection.each(function(param) {
          that.views.push(new jParameters.ParamView({
            model : param,
            tagName : 'li'
          }));
        });
    },
    
    render: function(){
        var that = this;
        // Clear out this element.
        $(this.el).empty();

        // Render each sub-view and append it to the parent view's element.
        _(this.views).each(function(dv) {
          $(that.el).append(dv.render().el);
        });
        return this;
        }
});    
    
    
    
    
    
    
    
    
    
//build backbone view
jParameters.GroupView = Backbone.View.extend({
    el: '<div></div>',
    template : templates.group,
    
    
    initialize: function(){
        if(this.model) {
              this.model.on('change',this.render,this);
              console.log(this.model);
            
            
            this.paramView = new jParameters.ParamSetView({collection : this.model.parameters});
            this.groupsView = new jParameters.GroupSetView({collection : this.model.subGroups});

        }
        this.render();
    },
    
    render: function(){
        if (this.model){
            var template = this.template({name : this.model.get("name"),  description : this.model.get("description")});
        this.$el.html(template);
        
        //display 
        var propEl = this.$el.find(".props");
        var subEl = this.$el.find(".sub");
        propEl.append(this.paramView.render().el);
        subEl.append(this.groupsView.render().el);
            
        } else {
            this.$el.html("no list loaded yet");
        }
        return this;
    }
});    
    
   
jParameters.GroupSetView = Backbone.View.extend({
    el: '<div></div>',
 
    initialize: function(){
        var that = this;
        this.views = [];

        this.collection.each(function(g) {
          that.views.push(new jParameters.GroupView({
            model : g,
            tagName : 'li'
          }));
        });
    },
    
    render: function(){
        var that = this;
        // Clear out this element.
        $(this.el).empty();

        // Render each sub-view and append it to the parent view's element.
        _(this.views).each(function(dv) {
          $(that.el).append(dv.render().el);
        });
        return this;
        }
});        
    
    

jParameters.AppView = Backbone.View.extend({
    el: '#list',
    template : templates.group,
    
    initialize: function(){
        if(this.model) {
              this.model.on('change',this.render,this);
              console.log(this.model);
            
            
            this.paramView = new jParameters.ParamSetView({collection : this.model.parameters});
            this.groupsView = new jParameters.GroupSetView({collection : this.model.subGroups});

        }
        this.render();
    },
    
    render: function(){
        if (this.model){
            var template = this.template({name : this.model.get("name"),  description : this.model.get("description")});
        this.$el.html(template);
        
        //display 
        var propEl = this.$el.find(".props");
        var subEl = this.$el.find(".sub");
        propEl.append(this.paramView.render().el);
        subEl.append(this.groupsView.render().el);
            
        } else {
            this.$el.html("no list loaded yet");
        }
        return this;
    }
});       
    
    
    
    
    
    
    
    
    
    
    
    
    
    
jParameters.getView = function (qualifiedName, name) {

    if (name) { //append name to the given path
        qualifiedName = jParameters.createQualifiedName(qualifiedName, name);
    }
    
    var dom = $("#" + qualifiedName);
    var view = dom.data("node");

    return view;
};




jParameters.cloneTemplate = function (queryClass) {
	//get the corresponding template for this nodes (e.g group)
	var t = $("#templates " + queryClass + ":first");
	if (t.length === 0) { return null; } //no template available

	var ni = t.clone();

    return ni;
};

jParameters.paramView = function (dom, model, mo, pn, desc, rootQualifiedName) {
    this.domNode = dom;
    this.modelObj = mo; //current model value
    this.originalValue = mo[pn]; //inital value
    
    this.dataModel = model;

    this.description = (desc) ? desc[0] : "";
    this.tooltip = (desc) ? desc[1] : "";
    this.name = pn;

    this.qualifiedName = jParameters.createQualifiedName(rootQualifiedName, this.name); //the full qualified name
    
    console.log("adding " + this.qualifiedName);
    
    //determine type (string, bool, num)
    
    this.status = { display : "full", edit : false, enabled : true };
    
    
    if (typeof this.originalValue !== "string"){
        this.status.enabled = false; //currently we handle only string
    }
    
    //attach me to the dom
    this.domNode.data("node", this);
    this.domNode.attr("id", this.qualifiedName);

};


jParameters.paramView.prototype.fill = function (docNode) {

    docNode.html(docNode.html().replace(/_name_/g, this.name));
	docNode.html(docNode.html().replace(/_value_/g, this.modelObj[this.name]));
    docNode.html(docNode.html().replace(/_description_/g, this.description));

};

jParameters.paramView.prototype.fillEdit = function (docNode) {

    if (!this.status.enabled){ 
        return;
    }
    
    //edit components
	var editPanel = docNode.find(".editPanel:last");
	var valueNode = docNode.find(".value");
    
        
    var inT = jParameters.cloneTemplate(".stringValue");
    inT.appendTo(editPanel.find(".inputDiv"));
    this.fill(inT);
    
    editPanel.hide(); //initially hidden
    var that = this;
    var enableAction = function(evt) {
        if (!that.status.edit){
        that.setEditEnabled(true);
        }
    };
    //valueNode.click(enableAction);
    valueNode.on("focus", enableAction);
    
    var doneFunction = function(evt) {
        if (that.status.edit) {
            that.modelObj[that.name] = editPanel.find("input").val();
            valueNode.text(that.modelObj[that.name]);
            that.setEditEnabled(false);
        }
    };
    
    editPanel.find(".doneIcon").on("click", doneFunction);
    editPanel.find("input")
        .on("blur",  doneFunction)
        .enterKey(doneFunction)
        .on('keyup', function (e) {
            if (e.which === 27) {
                editPanel.find("input").val( that.modelObj[that.name]); //reset input field
                that.setEditEnabled(false);
            }
        });    
};

jParameters.paramView.prototype.setEditEnabled = function (set) {
    
    this.status.edit = set;
    
    var editPanel = this.domNode.find(".editPanel:last");
	var valueNode = this.domNode.find(".value"); 
  
    if (set) {
		valueNode.hide();
        editPanel.show();
        editPanel.find("input").focus();
	} else {
		editPanel.hide();
	    valueNode.show();
        
        if (this.modelObj[this.name] !== this.originalValue){
            valueNode.addClass("modified");
        } else {
            valueNode.removeClass("modified");
        }
        
    }
    
};

jParameters.paramView.prototype.display = function () {

    this.domNode.html(""); //reset current view
    
    var t = jParameters.cloneTemplate(".parameter");
    this.fill(t);

    t.appendTo(this.domNode);
    this.fillEdit(t);
    
    var nameNode = t.find(".name");
    var valueNode = t.find(".value");

    if (this.tooltip !== "") {
        nameNode.tooltipster({ content: $('<span>'+ this.tooltip +'</span>') }); //
  
    }   // nameNode.tooltip();
      
    if (this.status.display === "hidden") {
        this.domNode.hide();
    }
    
    if (this.status.enabled == false){
        nameNode.addClass("disabled");
        valueNode.addClass("disabled");
        valueNode.removeAttr("tabIndex");
    }

    this.setEditEnabled(this.status.edit & this.status.enabled);
};



jParameters.paramView.prototype.filterVisible = function(filterQuery) {
    var visible;
        
    if (this.name.match(filterQuery)) {
       visible = true;
    } else { 
        visible = false;
    }
        
    if (!visible) {
      this.status.display = "hidden";
      this.domNode.hide(200);
    } else {
        this.status.display = "full";
        this.domNode.show(200);
    
    }
    return visible;
};


jParameters.groupView = function (dom,  model, mo, pn, rootQualifiedName) {
    this.domNode = dom;
    this.dataModel = model;
    this.modelObj = mo;
    this.name = pn;
    this.description = "";
    this.tooltip = "";
    
    this.qualifiedName = jParameters.createQualifiedName(rootQualifiedName, this.name); //the full qualified name
    
    console.log("adding " + this.qualifiedName);
    
    
    
    
    this.status = { display : "full", edit : false };
    
    //attach me to the dom
    this.domNode.data("node", this);
    this.domNode.attr("id", this.qualifiedName);

};


jParameters.groupView.prototype.fill = function (docNode) {

    docNode.html(docNode.html().replace(/_name_/g, this.name));
    docNode.html(docNode.html().replace(/_description_/g, this.description));

};

jParameters.groupView.prototype.display = function () {

    this.domNode.html(""); //reset current view
    var that = this;
    var t = jParameters.cloneTemplate(".group");
    
    
    var subGroups = [];
    var properties = [];
    var descriptions = [];
    $.each(this.modelObj, function (name, value) {
        var pair;
        if ($.isPlainObject(value)) {
            pair = { "name" : name, "group" : value};
            subGroups.push(pair);
        } else {
          //special case for descriptions
          if (name.match(/_/) && typeof value === "string"){
            var iii = value.search("\\. ");
            var valueShort = (iii > 0)? value.substr(0, iii) : value;
            value = value.replace(". ", ".<br/>");
            
            if (name === "_description"){ //this is the group description
                that.description = valueShort;
                that.tooltip = value;
               // t.find(".header").addClass("tooltip");
               // t.find(".header").attr("title", value);
             
            } else {
                var baseName = name.replace("_description", "");
                descriptions[baseName] = [valueShort, value];
            }
        } else {
            pair = { "name" : name, "value" : value};
            properties.push(pair);
        }
      }
    });
    
    
    this.fill(t);
    t.appendTo(this.domNode);
	if (this.tooltip !== ""){
       t.find(".header").tooltipster({ content: $('<span>'+ this.tooltip +'</span>') }); //
    }
    //set openable
    var oc = t.find(".collapseIcon");
    oc.click(function(evt) {
        that.status.display = (that.status.display === "full")? "close" : "full";
        that.display(); 
    });
    
    var isClosed = this.status.display === "close"; 
    if (isClosed){
        oc.addClass("ui-icon-circle-plus");
    } else { 
        oc.addClass("ui-icon-circle-minus");
    }
    
    
    
    
    //go through the property first
    var propsDom = this.domNode.find(".props:first");
    
    var l, rootLi, pv;
    
    for (l = 0; l < properties.length; l += 1) {
        rootLi = $("<div />");
        rootLi.appendTo(propsDom);
        var n = properties[l].name;
        pv = new jParameters.paramView(rootLi, this.dataModel, this.modelObj, n, descriptions[n], this.qualifiedName);
        if (isClosed){
            pv.status.display = "hidden";
        }
        pv.display();
    }
    
    //then list sub groups
    var subDom = this.domNode.find(".sub:first");
    for (l = 0; l < subGroups.length; l+=1) {
        rootLi = $("<div />");
        rootLi.appendTo(subDom);
        pv = new jParameters.groupView(rootLi, this.dataModel, subGroups[l].group,  subGroups[l].name, this.qualifiedName);
        if (isClosed){
            pv.status.display = "hidden";
        }
        pv.display();
    }
    
    
    
   
    
    if (this.status.display === "hidden"){
        this.domNode.hide();
    }
};



jParameters.groupView.prototype.filterVisible = function( filterQuery ){
    var rootName = this.qualifiedName;
    var visible = this.name.match(filterQuery);
    if (visible) {
        filterQuery = new RegExp(""); //if visible all child will be
    }
    
    $.each(this.modelObj, function(name, val) {
        if (name.match(/_description/)){
            return; //skip comments
        }
        var qName = jParameters.createQualifiedName(rootName, name);
            
        //set visible
        var pview = jParameters.getView(qName);
        var pvisible = pview.filterVisible(filterQuery);
            
        
        if (pvisible){
            visible = true;
        }
    });
    
  if (!visible){
    this.status.display = "hidden";
    this.domNode.hide(200);
  } else {
    this.status.display = "full";
    this.domNode.show(200);
    
  }
  return visible;

};











jParameters.display = function (dataModel) {

    //clear all
    var domRoot = $(".list:first");
    domRoot.html("");

    //create a groupView for the root
    var v = new jParameters.groupView(domRoot, dataModel, dataModel.data, "Parameters", "");

    v.display();
  
    v.filterVisible(new RegExp(dataModel.filterQuery));
};

jParameters.init = function(){


    if (chrome && chrome.fileSystem){
      jParameters.initChromeInOut($("#inout"));
    } else {
      jParameters.initInOut($("#inout"));
    }
    
    
    jParameters.initSearch($(".searchElement")); 
  

    $(document).ready(function() {
        $('.tooltip').tooltipster();
    });
    
    
   // jParameters.dataModel = new jParameters.model({});
};

///////////

jParameters.initSearch = function(searchNode){

    searchNode.keyup(function(evt){
        
        var searchString = searchNode.val();
        try{
            var searchQuery = new RegExp(searchString, 'i');
            jParameters.dataModel.filterQuery = searchQuery;
        }catch (e) {
            console.log("bad filter found");    
        }
                    
       // jParameters.display(jParameters.dataModel);
        var v = jParameters.getView("Parameters");
        v.filterVisible( new RegExp(jParameters.dataModel.filterQuery));
    });




};

jParameters.initInOut = function(domNode) {
	
    var loadFile = function(files){
        var reader = new FileReader();
		reader.onload = function(){
            try{
                var data = this.result;
                var structData = JSON.parse(data);
                var m =  new jParameters.model(structData);
                jParameters.dataModel = m;
			
			    console.log(data);
                
			    jParameters.display(jParameters.dataModel);
                
			} catch (e) {
				
                console.log(e);
				alert("the provided file is not a valid json parameter structure");
				throw e;
                
			}
		};
        
        jParameters.currentFile = files[0];
		reader.readAsText(files[0]);
    };
    
    
	domNode.children(".loadFile").change(function(evt) {
       
		loadFile(evt.target.files);
	});
	
	domNode.children(".load").click(function() {
		//pass request to file input
		$("#loadFile").trigger("click");
	});
	
	domNode.children(".save").click(function() {

		var stringOut = JSON.stringify(jParameters.dataModel.data);
		
		var blob = new Blob([stringOut], {type: "application/text;charset=" + document.characterSet});
		//var blob = new Blob([stringOut], {type: "application/x-lzip;charset=" + "UTF-16"}	);
		
		saveAs(blob, "params.json");

	
        //update original data
        jParameters.display(jParameters.dataModel);	
        
        var alert =  domNode.find(".alert");
        alert.removeClass("invisible");
        alert.fadeIn(100).delay(1200).fadeOut(300);
        
	});
	
    domNode.children(".overwrite").hide();
	
    
    //setup dropzone
    var obj = $("#dropzone");
    obj.on('dragenter', function (e) 
    {
        e.stopPropagation();
        e.preventDefault();
         $(this).removeClass().addClass("dropActive");

    });
     obj.on('dragleave', function (e) 
    {
        e.stopPropagation();
        e.preventDefault();
        $(this).removeClass().addClass("dropVisible");

    });
    
    obj.on('dragover', function (e) 
    {
         e.stopPropagation();
         e.preventDefault();
    });
    obj.on('drop', function (e) 
    {
        $(this).removeClass().addClass("dropInactive");
        e.preventDefault();
        e.stopPropagation();
        loadFile( e.originalEvent.dataTransfer.files );
    });

    
    $(document).on('dragenter', function (e) 
    {
        e.stopPropagation();
        e.preventDefault();
        obj.removeClass().addClass("dropVisible");
       
    });
    $(document).on('dragleave', function (e) 
    {
        e.stopPropagation();
        e.preventDefault();
    
    });
    $(document).on('dragover', function (e) 
    {
      e.stopPropagation();
      e.preventDefault();

    });
    $(document).on('drop', function (e) 
    {
        e.stopPropagation();
        e.preventDefault();
        obj.removeClass().addClass("dropInactive");
    });
    
    
};


jParameters.initChromeInOut = function(domNode) {
	
    var loadFile = function(files){
        var reader = new FileReader();
		reader.onload = function(){
            try{
                var data = this.result;
                var structData = JSON.parse(data);
                var m =  new jParameters.model(structData);
                jParameters.dataModel = m;
			
			    console.log(data);
                
			    jParameters.display(jParameters.dataModel);
                
			} catch (e) {
				
                console.log(e);
				alert("the provided file is not a valid json parameter structure");
				throw e;
                
			}
		};
        
        document.currentFile = files[0];
		reader.readAsText(files[0]);
    };
    
    
    var chosenFileEntry = null;

	
	domNode.children(".load").click(function(e) {
        chrome.fileSystem.chooseEntry({type: 'openFile'}, function(readOnlyEntry) {
            chosenFileEntry = readOnlyEntry;
            readOnlyEntry.file(function(file) {
                loadFile([file]);
            });
	   });
    });
        
	
	domNode.children(".save").click(function() {

		var stringOut = JSON.stringify(jParameters.dataModel.data, null, 2);
	
        
        chrome.fileSystem.chooseEntry({type: 'saveFile'}, function(writableFileEntry) {
            writableFileEntry.createWriter(function(writer) {
             // writer.onerror = errorHandler;
              writer.onwriteend = function(e) {
                console.log('write complete');
              };
              writer.write(new Blob([stringOut], {type: 'text/plain'}));
                
            
            });
        });
        
        //update original data
        jParameters.display(jParameters.dataModel);	
        
        var alert =  domNode.find(".alert");
        alert.removeClass("invisible");
        alert.fadeIn(100).delay(1200).fadeOut(300);

	
	});
	
    var overwriteFunc = function(){
        var stringOut = JSON.stringify(jParameters.dataModel.data, null, 2);
        
         chrome.fileSystem.getWritableEntry(chosenFileEntry, function(writableFileEntry) {
            writableFileEntry.createWriter(function(writer) {
             // writer.onerror = errorHandler;
              writer.onwriteend = function(e) {
                if (writer.length === 0) { //we have truncated the file. Write now...
                  writer.write(new Blob([stringOut], {type: 'text/plain'}));
                } else {
                    console.log('write complete');
                }
              };

            chosenFileEntry.file(function(file) {
              writer.truncate(0); //firstly clear file
            });
                  
          });
        });
        
	
        //update original data
        jParameters.display(jParameters.dataModel);	
        
        var alert =  domNode.find(".alert");
        alert.removeClass("invisible");
        alert.fadeIn(100).delay(1200).fadeOut(300);
    
    };
    
    domNode.children(".overwrite").click(overwriteFunc);
	
    
    $(document).keypress("s",function(e) {
      if(e.ctrlKey && chosenFileEntry !== null ) {
        overwriteFunc();
      }
    });
    
    
    //setup dropzone
    var obj = $("#dropzone");
    obj.on('dragenter', function (e) 
    {
        e.stopPropagation();
        e.preventDefault();
         $(this).removeClass().addClass("dropActive");

    });
     obj.on('dragleave', function (e) 
    {
        e.stopPropagation();
        e.preventDefault();
        $(this).removeClass().addClass("dropVisible");

    });
    
    obj.on('dragover', function (e) 
    {
         e.stopPropagation();
         e.preventDefault();
    });
    obj.on('drop', function (e) 
    {
        $(this).removeClass().addClass("dropInactive");
        e.preventDefault();
        e.stopPropagation();
        var F = e.originalEvent.dataTransfer.items[0];
        
        chosenFileEntry = F.webkitGetAsEntry();
        loadFile( e.originalEvent.dataTransfer.files );
    });

    
    $(document).on('dragenter', function (e) 
    {
        e.stopPropagation();
        e.preventDefault();
        obj.removeClass().addClass("dropVisible");
       
    });
    $(document).on('dragleave', function (e) 
    {
        e.stopPropagation();
        e.preventDefault();
    
    });
    $(document).on('dragover', function (e) 
    {
      e.stopPropagation();
      e.preventDefault();

    });
    $(document).on('drop', function (e) 
    {
        e.stopPropagation();
        e.preventDefault();
        obj.removeClass().addClass("dropInactive");
    });
    
    
};

    
    
    

/////////////////////finally initalize document
//jParameters.initTemplatesR();
jParameters.init();    

return jParameters;
});
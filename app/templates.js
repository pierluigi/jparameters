/*global define */
/*jslint white: true */
/*jslint vars: true*/

define(['tpl!templates/group.tpl',
        'tpl!templates/param.tpl'],
    function(gt, pt){

        'use strict';

        var templates = {};

        templates.group = gt;    
        templates.param = pt;    

        return templates;
    }
);

// third party dependencies are in the lib folder.
// app 
// Configure loading modules from the lib directory,
// except for 'app' ones, which are in a sibling
// directory.
requirejs.config({
    baseUrl: 'libs',
    paths: {
        'text':     'requirejs-text/text',
        'tpl':     'requirejs-tpl/tpl',
        'jquery' : 'jquery/dist/jquery',
        'jquery-ui' : 'jquery-ui/jquery-ui',
        'FileSaver' : 'FileSaver/FileSaver',
        'tooltipster' : 'tooltipster/js/jquery.tooltipster',
        'backbone' : 'backbone/backbone',
        'backbone-localstorage' : 'backbone-localstorage/backbone-localstorage',
        'underscore': 'underscore/underscore',
        'lz-string': 'lz-string/libs/lz-string',
        'q' : 'q/q',
        
        app: '../app',
        templates: '../templates'
    },
    shim: {
        'backbone': {
            //These script dependencies should be loaded before loading backbone.js
            deps: ['underscore', 'jquery'],
            //Once loaded, use the global 'Backbone' as the module value.
            exports: 'Backbone'
        },
        'backbone-localstorage' : ['backbone'],
        'tooltipster' : ['jquery'],
        'underscore': {
            exports: '_'
        }
    }
});

// Start loading the main app file. All application logic is in there.
require([
    'text',
    'jquery',
    'jquery-ui',
    'FileSaver',
    'lz-string',
    'tooltipster',
    'underscore',
    'backbone',
    'backbone-localstorage',

    
    'app/jParametersBB', 
    'app/main'
]);
   
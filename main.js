/**
 * Listens for the app launching then creates the window
 *
 * @see http://developer.chrome.com/apps/app.runtime.html
 * @see http://developer.chrome.com/apps/app.window.html
 */
chrome.app.runtime.onLaunched.addListener(function() {
  // Center window on screen.
  var screenWidth = screen.availWidth;
  var screenHeight = screen.availHeight;
  var w = 1400;
  var h = 800;

  chrome.app.window.create('index.html', {
    id: "jParametersID",
    outerBounds: {
      width: w,
      height: h,
      left: Math.round((screenWidth-w)/2),
      top: Math.round((screenHeight-h)/2)
    }
  });
  
});
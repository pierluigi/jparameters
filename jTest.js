/*global $, jParameters*/
$(function () {
    'use strict';

  
    var testParams = {
        _description : "a bunch of strange parameters",
        Fruits : {
            _description : "fruits related parameters. This is going to be a very long description so we split it into pices",

            selectedFruit : "pear",
            numbers : [1, 3, 4],
            color : "yellow",
            prices : { pear : 5, apple : 2 }
        },
        Vegetables : {
            _description : "veggies related parameters",
            selectedVeggie : "carrots",
            color : "orange",
            color_description : "A simple color. In detail this is a color of a specific vegetable",
            prices : { spinach : 5, carrots : 2 }
        },
        preferredMeal : "fruits",
        preferredColor : "orange",


        preferredMeal_description : "your preferred choice",
        preferredColor_description : "your preferred color"
    };





    jParameters.dataModel = new jParameters.model(testParams);
    jParameters.display(jParameters.dataModel);
    
});